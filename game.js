var prompt = require('sync-prompt').prompt;
var colors = require('colors');

(function js_game () {
  
  var bank = 100
  var bet;
  var answer;

  function get_bet() {
    return parseInt(prompt("Please place a bet between $5 and $10 \n"));
  }

  function check_bet (x) {
    if (5 <= x && x <= 10) {
      return x;
    } else {
      input = get_bet();
      return check_bet(input);
    }
  }

  function get_guess () {
    return parseInt(prompt("Guess a number between 1 and 10 \n"));
  }

  function check_guess (x) {
    if (1 <= x && x <= 10) {
      return x;
    } else {
      input = get_guess();
      return check_guess(input);
    }
  }

  function generate_number () {
    return Math.floor((Math.random() * 10) + 1);
  }

  function verify_answer (guess, number) {
    if (guess === number) {
      console.log("Correct!".green);
      correct_answer(bet);
    } else if (guess + 1 === number || guess - 1 === number) {
      console.log("Off by one!".white);
    } else {
      console.log("Incorrect!".red);
      incorrect_answer(bet);
    }
  }

  function correct_answer (bet) {
    return bank = bank + (bet * 2);
  }

  function incorrect_answer (bet) {
    return bank = bank - (bet);
  }

  function play () {
    inputted_bet = get_bet();
    bet = check_bet(inputted_bet);
    number = generate_number()
    inputted_guess = get_guess();
    guess = check_guess(inputted_guess);
    verify = verify_answer(guess, number);
    console.log("You're bank balance is " + bank);
    if (bank > 0) {
    play();
    }
  }

  play();
  

})();